#include <PubSubClient.h>
#include <NewPing.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <DHT.h>
#include <DHT_U.h>

// Instanca DHT senzora.
DHT dht(4, DHT11);
// Instanca HC senzora.
NewPing hc(16, 15);

// Klijent za spajanje na MQTT server
WiFiClient wificlient;
PubSubClient mqttclient(wificlient);

float temperatura;
float vlaga;
float udaljenost;

String clientid = "ESP8266DPetrec-01";

void setup() {
  // Započni serijsku komunikaciju.
  Serial.begin(115200);
  // Započni Wi-Fi vezu.
  WiFi.begin("Birtija Netvorking", "hladnopivoiburek");
  // Čekaj dok se Wi-Fi veza ne ostvari
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  // Ispiši IP adresu mikroupravljača
  Serial.println(WiFi.localIP());

  // Postavljanje MQTT servera
  mqttclient.setServer("mqtt.vub.zone", 1883);
}

void loop() {
  // Varijable za podatke
  temperatura = dht.readTemperature();
  vlaga = dht.readHumidity();
  udaljenost = hc.ping_cm();

  // Ispis podataka
  Serial.printf("T: %f V: %f U: %f\n", temperatura, vlaga, udaljenost);

  // Spajanje na MQTT
  if (mqttclient.connect(clientid.c_str(), "iot-ws-dev", "M9W7SAWrg_JZ")) {
    Serial.println("Uspjesno spajanje na MQTT");
  } else {
    Serial.println("Neuspjesno spajanje na MQTT");
  }

  // Objava podataka MQTT serveru
  mqttclient.publish("iot/dpetrec/temperatura", String(temperatura).c_str());
  mqttclient.publish("iot/dpetrec/vlaga", String(vlaga).c_str());
  mqttclient.publish("iot/dpetrec/udaljenost", String(udaljenost).c_str());

  Serial.println("Poslano na MQTT");
  
  delay(1000);

  // Održavanje veze s MQTT serverom
  mqttclient.loop();
}
